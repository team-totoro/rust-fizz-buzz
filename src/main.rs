// Rohmer Maxime 
// 22/04/2022
// My FizzBuzz interpretation in Rust
// Usage: ./FizzBuzz 
const MIN:u32 = 1;
const MAX:u32 = 50;
const FIZZ_DIVIDER:u32 = 3;
const BUZZ_DIVIDER:u32 = 5;
const FIZZ_MESSAGE: &str = "Fizz";
const BUZZ_MESSAGE: &str = "Buzz";
fn main() {
    for current_number in MIN..(MAX + 1){
        let mut result = String::from("");
        if current_number % FIZZ_DIVIDER != 0 && current_number % BUZZ_DIVIDER != 0{
            result = format!("{}",current_number);
        }else{
            if current_number % FIZZ_DIVIDER == 0{
                result.push_str(FIZZ_MESSAGE);
            }
            if current_number % BUZZ_DIVIDER == 0{
                result.push_str(BUZZ_MESSAGE);
            }
        }
        println!("{}",result);
    }
}
